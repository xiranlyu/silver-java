package com.tw;

public class SmartParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        ParkingResult result = new ParkingResult("");
        ParkingLot largestParkingLot = new ParkingLot();
        int maxPositions= 0;
        for (ParkingLot parkingLot: getParkingLots()) {
            if (parkingLot.getAvailableParkingPosition() >= maxPositions) {
                maxPositions = parkingLot.getAvailableParkingPosition();
                largestParkingLot = parkingLot;
            }
        }
        try {
            result = new ParkingResult(largestParkingLot.park(car));
        } catch (ParkingLotFullException ignored) {
            setLastErrorMessage("The parking lot is full.");
        }
        return result.getTicket();
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        FetchingResult result = new FetchingResult("");
        for (ParkingLot parkingLot: getParkingLots()) {
            try {
                result = new FetchingResult(parkingLot.fetch(ticket));
                setLastErrorMessage(result.getMessage());
                break;
            } catch (InvalidParkingTicketException ignored) {
                setLastErrorMessage("Unrecognized parking ticket.");
            } catch (NullPointerException ignored) {
                setLastErrorMessage("Please provide your parking ticket.");
            }
        }

        return result.getCar();
    }
    // --end->
}
