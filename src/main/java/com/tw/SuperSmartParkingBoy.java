package com.tw;

public class SuperSmartParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        ParkingResult result = new ParkingResult("");
        ParkingLot highestRateLot = new ParkingLot();
        double maxRate = 0.0;
        for (ParkingLot parkingLot: getParkingLots()) {
            double available = parkingLot.getAvailableParkingPosition();
            double capacity = parkingLot.getCapacity();
            double rate = available / capacity;
            if (rate >= maxRate) {
                maxRate = rate;
                highestRateLot = parkingLot;
            }
        }
            try {
                result = new ParkingResult(highestRateLot.park(car));
            } catch (ParkingLotFullException ignored) {
                setLastErrorMessage("The parking lot is full.");
            }
        return result.getTicket();
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        FetchingResult result = new FetchingResult("");
        for (ParkingLot parkingLot: getParkingLots()) {
            try {
                result = new FetchingResult(parkingLot.fetch(ticket));
                setLastErrorMessage(result.getMessage());
                break;
            } catch (InvalidParkingTicketException ignored) {
                setLastErrorMessage("Unrecognized parking ticket.");
            } catch (NullPointerException ignored) {
                setLastErrorMessage("Please provide your parking ticket.");
            }
        }

        return result.getCar();
    }
    // --end->
}
